import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({
    title: `${fighter.name} is winner`,
    bodyElement: createFighterImage(fighter)
  })
}

import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    let additionalForFirstFighterAttack = {
      attackerControl: controls.PlayerOneBlock,
      defenderControl: controls.PlayerTwoBlock,
      maxDefenderHealth: secondFighter.health,
      indicatorElement: document.getElementById('right-fighter-indicator'),
      indicatorWidth: parseFloat(getComputedStyle(document.getElementById('right-fighter-indicator')).width),
      hitMultiplier: 1
    }

    let additionalForSecondFighterAttack = {
      attackerControl: controls.PlayerTwoBlock,
      defenderControl: controls.PlayerOneBlock,
      maxDefenderHealth: firstFighter.health,
      indicatorElement: document.getElementById('left-fighter-indicator'),
      indicatorWidth: parseFloat(getComputedStyle(document.getElementById('left-fighter-indicator')).width),
      hitMultiplier: 1
    }

    let keysForBlock = [];

    let keysForCriticalHit = {};

    let firstFighterLastCriticalHit = 0;
    let secondFighterLastCriticalHit = 0

    document.addEventListener('keydown', attackHandler);
    document.addEventListener('keyup', () => keysForCriticalHit = {})

    function attackHandler(event) {
      switch (event.code) {
        case controls.PlayerOneAttack:
          attack(firstFighter, secondFighter, additionalForFirstFighterAttack);
          break;
        case controls.PlayerTwoAttack:
          attack(secondFighter, firstFighter, additionalForSecondFighterAttack)
          break;
        case controls.PlayerOneBlock:
          block(controls.PlayerOneBlock);
          break;
        case controls.PlayerTwoBlock:
          block(controls.PlayerTwoBlock);
          break;
        default:
          criticalHit(event.code);
      }
    }

    function criticalHit(code) {
      keysForCriticalHit[code] = true;
      if (keysForCriticalHit[controls.PlayerOneCriticalHitCombination[0]] && keysForCriticalHit[controls.PlayerOneCriticalHitCombination[1]] && keysForCriticalHit[controls.PlayerOneCriticalHitCombination[2]]) {
        const now = new Date();
        if (now - firstFighterLastCriticalHit > 10000) {
          firstFighterLastCriticalHit = now;
          additionalForFirstFighterAttack.hitMultiplier = 2;
          attack(firstFighter, secondFighter, additionalForFirstFighterAttack);
          additionalForFirstFighterAttack.hitMultiplier = 1;
        }
      } else if (keysForCriticalHit[controls.PlayerTwoCriticalHitCombination[0]] && keysForCriticalHit[controls.PlayerTwoCriticalHitCombination[1]] && keysForCriticalHit[controls.PlayerTwoCriticalHitCombination[2]]) {
        const now = new Date();
        if (now - secondFighterLastCriticalHit > 10000) {
          secondFighterLastCriticalHit = now;
          additionalForSecondFighterAttack.hitMultiplier = 2;
          attack(secondFighter, firstFighter, additionalForSecondFighterAttack);
          additionalForSecondFighterAttack.hitMultiplier = 1;
        }
      }
    }

    function attack(attacker, defender, additional) {
      if (keysForBlock.findIndex(el => el === additional.attackerControl) === -1) {
        const damage = getDamage(attacker, defender) * additional.hitMultiplier;
        const indicatorElement = additional.indicatorElement;

        if (keysForBlock.findIndex(el => el === additional.defenderControl) === -1) {
          if (defender.health >= 0 && defender.health >= damage) {
            defender.health -= damage;
            let fighterHealthPercentage = defender.health * 100 / additional.maxDefenderHealth;
            indicatorElement.style.width = `${fighterHealthPercentage / 100 * additional.indicatorWidth}px`
          } else {
            indicatorElement.style.width = '0';
            document.removeEventListener('keydown', attackHandler);
            resolve(attacker);
          }
        }
      }
    }

    function block(blockControl) {
      let index = keysForBlock.findIndex(el => el === blockControl)
      if (index > -1) {
        keysForBlock.splice(index, 1);
      } else {
        keysForBlock.push(blockControl);
      }
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower > blockPower ? hitPower - blockPower : 0;

  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;

  return power;
}

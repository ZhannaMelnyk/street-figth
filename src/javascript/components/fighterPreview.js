import { createElement } from '../helpers/domHelper';
import { fighters, fightersDetails } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const fighterName = createElement({
      tagName: 'h2',
      className: `fighter_name`,
    })
  
    fighterName.innerText = `${fighter.name}`;
    const fighterDetails = createElement({
      tagName: 'div',
      className: `fighter_details`
    })
  
    const fighterHealth = createElement({
      tagName: 'p'
    })
    fighterHealth.innerText = `Health: ${fighter.health}`;
    const fighterAttack = createElement({
      tagName: 'p'
    })
    fighterAttack.innerText = `Attack: ${fighter.attack}`;
    const fighterDefense = createElement({
      tagName: 'p'
    })
    fighterDefense.innerText = `Defense: ${fighter.defense}`;
    
    fighterDetails.append(fighterHealth, fighterAttack, fighterDefense);
  
    const fighterImg = createFighterImage(fighter);
  
    fighterElement.append(fighterName, fighterDetails, fighterImg);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
